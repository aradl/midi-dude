#!/bin/bash
# requires package alsa-utils

# get ALSA MIDI device IDs
INPUT=`aconnect -i | grep "'Digital Piano'"  | awk '{print ($2+0)}'`
#OUTPUT=`aconnect -o | grep "'Moog Sub Phatty'"  | awk '{print ($2+0)}'`
OUTPUT=`aconnect -o | grep "'Elektron Digitakt'"  | awk '{print ($2+0)}'`


# make sure the return is not empty 
if [ -z "$INPUT" ] && [ -z "$OUTPUT" ] ; then
  echo "no ID found"; exit 1
fi

# make sure the return is a number
re='^[0-9]+$'
if ! [[ $INPUT =~ $re ]] ; then
   echo "NaN" >&2; exit 1
fi

# connect!
eval "aconnect $INPUT $OUTPUT"
echo "Piano ($INPUT) and Digitakt ($OUTPUT) are Connected!"

